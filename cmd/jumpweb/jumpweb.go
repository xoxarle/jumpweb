package main

import (
	"fmt"
	"os"
	"time"

	warp "codeberg.org/xoxarle/jumpweb/jumpweb"
)

func main() {
	fmt.Println("Warp mapper ready to GO :-) ")
	if len(os.Args) >= 2 {
		warpFile := os.Args[1]
		shouldPrint := len(os.Args) > 2 && os.Args[2] == "ok"
		sm, err := warp.StarmapFromFile(warpFile)
		if err == nil {
			tm := time.Now()
			_ = sm.CalculateNeighbors(7.7)
			elapsed := time.Since(tm).Milliseconds()
			fmt.Printf("Neigbors calculated in %d millisec\n", elapsed)
			tm = time.Now()
			sm.BuildReachabilityMap(0, true)
			elapsed = time.Since(tm).Milliseconds()
			fmt.Printf("Map rebuilt in %d millisec\n", elapsed)

			if shouldPrint {
				for i, s := range sm.Stars {
					if len(s.NeighborsIdx) > 0 {
						if len(sm.Reachability[i].Path) > 0 {
							fmt.Printf("%d - %v\n", i, sm.Name)
							r := sm.Reachability[i]
							dist := 0.0
							for x := 1; x < len(r.Path); x++ {
								ix1 := r.Path[x-1]
								ix2 := r.Path[x]
								s1 := sm.Stars[ix1]
								s2 := sm.Stars[ix2]
								dist += s1.DistanceFrom(&s2.Coordinates)
								fmt.Printf("\t%s --> %s : %f\n", s1.Name, s2.Name, dist)
							}
						}
					}
				}
			}
		} else {
			fmt.Printf("%v", err)
		}
	} else {
		fmt.Println("Please provide a starmap file")
	}
}
