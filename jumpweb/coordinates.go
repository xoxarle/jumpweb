package jumpweb

import (
	"errors"
	"math"
	"strconv"
	"strings"
)

type Coordinates struct {
	X float64
	Y float64
	Z float64
}

func (c *Coordinates) DistanceFrom(c1 *Coordinates) float64 {
	return math.Sqrt(math.Pow(c1.X-c.X, 2) + math.Pow(c1.Y-c.Y, 2) + math.Pow(c1.Z-c.Z, 2))
}

type PVertex *Vertex

type Vertex struct {
	Coordinates
	Id           int
	Name         string
	Neighbors    []PVertex
	NeighborsIdx []int
	Visited      bool
	Reference    bool
	Distance     float64
}

func MakeVertex(newName string, newCoord Coordinates) Vertex {
	return Vertex{Name: newName, Coordinates: newCoord}
}

func (v *Vertex) AddNeighbor(v1 *Vertex, idx, idx1 int) {
	for _, vx := range v.Neighbors {
		if vx.Name == v1.Name {
			return
		}
	}

	v.Neighbors = append(v.Neighbors, v1)
	v.NeighborsIdx = append(v.NeighborsIdx, idx1)
	v1.Neighbors = append(v1.Neighbors, v)
	v1.NeighborsIdx = append(v1.NeighborsIdx, idx)
}

func VertexFromString(s string) (Vertex, error) {
	if s == "" {
		return Vertex{}, errors.New("Please provide a filled string")
	}
	v := Vertex{}
	arr := strings.Split(s, "/")
	if len(arr) < 7 {
		return Vertex{}, errors.New("There must be at least 7 elements")
	}
	var err error
	v.Id, err = strconv.Atoi(strings.Split(arr[0], ".")[0])

	item7 := arr[6]

	v.X, err = strconv.ParseFloat(arr[1], 64)
	v.Y, err = strconv.ParseFloat(arr[2], 64)
	v.Z, err = strconv.ParseFloat(arr[3], 64)
	if item7 == "" {
		v.Name = arr[4]
	} else {
		v.Name = arr[6]
	}
	return v, err

}
