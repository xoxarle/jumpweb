package jumpweb

import (
	"fmt"
	"math"
	"testing"

	dice "codeberg.org/xoxarle/dice/dice"
)

func TestCoordinates(t *testing.T) {
	t.Run("Check unity distance on axis", func(t *testing.T) {
		c1 := Coordinates{0.0, 0.0, 0.0}
		c2 := Coordinates{1.0, 0.0, 0.0}
		c3 := Coordinates{0.0, 1.0, 0.0}
		c4 := Coordinates{0.0, 0.0, 1.0}

		dist := c1.DistanceFrom(&c2)
		got := 1.0
		if dist != got {
			t.Errorf("Should get %f, got %f", dist, got)
		}

		dist = c1.DistanceFrom(&c3)
		if dist != got {
			t.Errorf("Should get %f, got %f", dist, got)
		}

		dist = c1.DistanceFrom(&c4)
		if dist != got {
			t.Errorf("Should get %f, got %f", dist, got)
		}

		c1 = Coordinates{1.0, 1.0, 1.0}
		c2 = Coordinates{3.0, 3.0, 3.0}
		dist = c1.DistanceFrom(&c2)
		got = math.Sqrt(6.0)

	})

}

func TestVertex(t *testing.T) {
	t.Run("Testing Vertex", func(t *testing.T) {
		v := Vertex{Name: "Beta Canum", Coordinates: Coordinates{1, 2, 3}}
		wants := "Beta Canum"
		if v.Name != wants {
			t.Errorf("Should get %s, got %s", v.Name, wants)
		}
	})

	t.Run("Testing MakeVertex", func(t *testing.T) {
		v := MakeVertex("Beta Canum", Coordinates{1, 2, 3})
		wants := "Beta Canum"
		if v.Name != wants {
			t.Errorf("Should get %s, got %s", v.Name, wants)
		}
		if len(v.Neighbors) != 0 {
			t.Errorf("Length of neighbors should be 0, it is %d", len(v.Neighbors))
		}
	})

	t.Run("Testing AddNeighbor", func(t *testing.T) {
		v := MakeVertex("Beta Canum", Coordinates{1, 2, 3})
		v1 := MakeVertex("Hochbaden", Coordinates{4, 5, 6})

		v.AddNeighbor(&v1, 0, 1)

		if v1.Name != v.Neighbors[0].Name {
			t.Errorf("Should get %s, got %s", v1.Name, v.Neighbors[0].Name)
		}
	})

	t.Run("Testing Neighbors, 2", func(t *testing.T) {
		var vertices []Vertex
		vertices = append(vertices, MakeVertex("Reference", Coordinates{0, 0, 0}))
		vertices = append(vertices, MakeVertex("Reference-1", Coordinates{2, 2, 2}))
		cnt := 750
		for x := 0; x < cnt-2; x++ {
			vertices = append(vertices, MakeVertex(dice.Word(3+dice.D8()), Coordinates{
				float64(dice.Dn(10000)/100.0 - 50.0),
				float64(dice.Dn(10000)/100.0 - 50.0),
				float64(dice.Dn(10000)/100.0 - 50.0)}))
		}
		for x := 0; x < cnt-1; x++ {
			v0 := &(vertices[x])
			for y := x + 1; y < cnt; y++ {
				v1 := &(vertices[y])
				dist := v0.DistanceFrom(&v1.Coordinates)
				if dist < 7 {
					v0.AddNeighbor(v1, x, y)
				}
			}
		}
		if len(vertices) != cnt {
			t.Errorf("Should have %d elements", cnt)
		}

		if len(vertices[0].Neighbors) < 2 {
			t.Errorf("Should have at least two elements %v", vertices[0])
		}

		for _, vx := range vertices {
			for _, vy := range vx.Neighbors {
				dist := vx.DistanceFrom(&vy.Coordinates)
				if dist > 7 {
					t.Errorf("Distance from %s to %s should be < 7, it is %f", vx.Name, vy.Name, dist)
				}
			}
		}
	})

	t.Run("Vertex from String", func(t *testing.T) {
		const VERTEX = "0080.10/25.50/0.70/42.20/Beta Cassiopei B////m0v/SB/13.00////8.00"

		_, err := VertexFromString("Soccmel")
		if err == nil {
			t.Errorf("VertexFromString(\"Soccmel\") should return error %v", err)
		} else {
			fmt.Printf("OK, I expected: %s\n", err)
		}

		_, err = VertexFromString("Soccmel/a/b/c")
		if err == nil {
			t.Errorf("VertexFromString(\"Soccmel\") should return error %v", err)
		} else {
			fmt.Printf("OK, I expected: %s\n", err)
		}

		_, err = VertexFromString("Soccmel/a/b/c/d/e/f")
		if err == nil {
			t.Errorf("VertexFromString(\"Soccmel\") should return error %v", err)
		} else {
			fmt.Printf("OK, I expected: %s\n", err)
		}

	})

}
