package jumpweb

import (
	"fmt"
	"testing"
	"time"
)

func TestStarmap(t *testing.T) {

	t.Run("Starmap loading", func(t *testing.T) {
		sm, err := StarmapFromFile("Helenejacq sector.str")
		want := "Helenejacq sector"
		if err != nil {
			t.Errorf("Loading from file should yield no error, instead... %s", err)
		}
		if sm.Name != want {
			t.Errorf("Loaded starmap's name must be %s instead is %s", sm.Name, want)
		}
		if len(sm.IdStarMap) < 300 {
			t.Errorf("There should be more than 300 unique systems mapped")
		}
		if len(sm.Stars) < 300 {
			t.Errorf("There should be more than 300 star systems")
		}
		start := time.Now()

		err = sm.CalculateNeighbors(7.7)
		elapsed := time.Since(start)
		fmt.Printf("\n\nElapsed seconds calc neighbors: %s\n\n", elapsed)
		start = time.Now()

		sm.BuildReachabilityMap(0, true)
		//sm.MainBuildReachabilityMap(0, true)
		if err != nil {
			t.Errorf("At this point, you should be able to calculate neighbors")
		}
		elapsed = time.Since(start)
		fmt.Printf("\n\nElapsed seconds reachability: %s\n\n", elapsed)

		if len(sm.Stars[0].Neighbors) < 1 {
			t.Errorf("Central star must have at least one star within 7.7ly range")
		}

		st := sm.Stars[0]
		st1 := sm.IdStarMap[14307]
		dist := st.DistanceFrom(&st1.Coordinates)
		fmt.Printf("%f %v %v\n", dist, st, st1)

		// found := false
		// for _, n := range st.neighbors {
		// 	fmt.Println(n.name)
		// 	if n.name == "Wolf 359" {
		// 		found = true
		// 	}
		// }
		// if !found {
		// 	t.Errorf("Wolf 359 must be within 7.7ly range %v", st)
		// }

		idx := sm.IdIndexMap[14307]
		rch := sm.Reachability[idx]
		fmt.Println("-----------------------------")
		for _, id := range rch.Path {
			fmt.Printf("%s\t%f\n", sm.Stars[id].Name, sm.Reachability[id].Distance)
		}
		fmt.Printf("\nDistance is %f\n", rch.Distance)
		fmt.Println("-----------------------------")

	})

	t.Run("Testing reachability vector substitution", func(t *testing.T) {
		//sm, err := StarmapFromFile("2300ad.txt")
		rv := ReachabilityVector{0.0, []int{1, 2, 3, 4, 5, 6, 7, 8, 9}}
		changeVect := []int{1, 6, 8}
		err := rv.SubstituteSubVector(changeVect)
		if rv.Path[0] != 1 || rv.Path[1] != 6 || rv.Path[2] != 8 || rv.Path[3] != 9 || len(rv.Path) != 4 || err != nil {
			t.Errorf("Failed changing subvector, want {1,6,8,9} got %v with err %v", rv.Path, err)
		}
	})

}
