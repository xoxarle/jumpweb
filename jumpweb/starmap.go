package jumpweb

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"sync"
)

type ReachabilityVector struct {
	Distance float64
	Path     []int
}

func (v *ReachabilityVector) GetPositionInPath(indexToCheck int) int {
	res := -1
	for idx, ix := range v.Path {
		if ix == indexToCheck {
			res = idx
			break
		}
	}
	return res
}

func (v *ReachabilityVector) SubstituteSubVector(subvect []int) error {
	if len(subvect) == 0 {
		return errors.New("Cannot change a null-length vector")
	}
	last := subvect[len(subvect)-1]
	pos_last := v.GetPositionInPath(last)
	if pos_last == -1 {
		return errors.New("Last element of new vector not present in path")

	}
	dest := []int{}
	for _, ix := range subvect {
		dest = append(dest, ix)
	}
	for ix := pos_last + 1; ix < len(v.Path); ix++ {
		dest = append(dest, v.Path[ix])
	}
	v.Path = dest
	return nil
}

type Starmap struct {
	mtx          sync.Mutex
	Name         string
	Stars        []*Vertex
	IdStarMap    map[int]*Vertex
	Reachability map[int]*ReachabilityVector
	IdIndexMap   map[int]int
}

func (s *Starmap) SetDistanceToVector(rv *ReachabilityVector, dist float64) {
	//s.mtx.Lock()
	//defer s.mtx.Unlock()
	rv.Distance = dist
}

func (s *Starmap) UpdateReachabilityVectorDistance(i int) error {
	//s.mtx.Lock()
	//defer s.mtx.Unlock()
	var err error
	err = nil
	rv, ok := s.Reachability[i]
	if !ok {
		return errors.New("Key out of range")
	}
	if len(rv.Path) > 0 {
		tmpDist := 0.0
		for ix := 0; ix < len(rv.Path)-1; ix++ {
			tmpDist += s.Stars[rv.Path[ix+1]].DistanceFrom(&s.Stars[rv.Path[ix]].Coordinates)
		}
		s.SetDistanceToVector(rv, tmpDist)
	} else {
		s.SetDistanceToVector(rv, 0.0)
	}
	return err

}

func StarmapFromFile(filename string) (*Starmap, error) {
	v := Starmap{}
	v.IdStarMap = make(map[int]*Vertex) //mo vé devo inizializzare la mappa
	lines, err := readLines(filename)
	if err != nil {
		return &Starmap{}, err
	}
	v.Name = lines[0]
	v.Reachability = make(map[int]*ReachabilityVector)
	v.IdIndexMap = make(map[int]int)
	cnt := 0
	for _, line := range lines[1:] {
		vx, err2 := VertexFromString(line)
		if err2 != nil {
			return &Starmap{}, err2
		}
		_, ok := v.IdStarMap[vx.Id]
		if !ok {
			v.Stars = append(v.Stars, &vx)
			v.IdStarMap[vx.Id] = (v.Stars[len(v.Stars)-1])
			v.Reachability[cnt] = &ReachabilityVector{}
			v.IdIndexMap[vx.Id] = cnt
			cnt++
		}
	}
	v.IdStarMap = make(map[int]*Vertex)

	// la primitiva range effettua copie delle struct.
	for _, star := range v.Stars {
		v.IdStarMap[star.Id] = star
	}
	return &v, nil
}

func (sm *Starmap) _CalculateNeighbors(mydist float64) error {
	cnt := len(sm.Stars)
	for x := 0; x < cnt-1; x++ {
		v0 := sm.Stars[x]
		for y := x + 1; y < cnt; y++ {
			v1 := sm.Stars[y]
			dist := v0.DistanceFrom(&v1.Coordinates)
			if dist < mydist {
				v0.AddNeighbor(v1, x, y)
			}
		}
	}

	return nil
}

func (sm *Starmap) CalculateNeighbors(mydist float64) error {
	cnt := len(sm.Stars)
	var wg sync.WaitGroup

	lockAddNeighbor := func(mtx *sync.Mutex, v0 *Vertex, v1 *Vertex, x int, y int) {
		mtx.Lock()
		defer mtx.Unlock()
		v0.AddNeighbor(v1, x, y)
	}

	var mtx sync.Mutex
	for x := 0; x < cnt-1; x++ {
		wg.Add(1)
		go func(x1 int) {
			v0 := sm.Stars[x1]
			for y := x1 + 1; y < cnt; y++ {
				v1 := sm.Stars[y]
				dist := v0.DistanceFrom(&v1.Coordinates)
				if dist < mydist {
					lockAddNeighbor(&mtx, v0, v1, x1, y)
					//v0.AddNeighbor(v1, x1, y)
				}
			}
			wg.Done()
		}(x)
	}
	wg.Wait()
	return nil
}

func (sm *Starmap) AppendToPath(pathIdx, idx int) error {
	//sm.mtx.Lock()
	starPath, ok := sm.Reachability[pathIdx]
	if !ok {
		return errors.New("Path not present")
	} else {
		starPath.Path = append(starPath.Path, idx)
	}
	//sm.mtx.Unlock()
	return nil
}

func (sm *Starmap) MainBuildReachabilityMap(idx int, isStartingNode bool) {

	wn1 := sm.Stars[idx]
	wn1.Visited = true
	var wg sync.WaitGroup
	for _, i := range wn1.NeighborsIdx {
		wg.Add(1)
		go func(ix int) {
			sm.BuildReachabilityMap(ix, true)
			wg.Done()
		}(i)
	}
	wg.Wait()
	for i := 0; i < len(sm.Stars); i++ {
		s := sm.Stars[i]
		if s.Id != wn1.Id {
			s.Reference = false
			rm := sm.Reachability[i]
			if len(rm.Path) > 0 {
				rm.Path = append([]int{idx}, rm.Path...)
				sm.UpdateReachabilityVectorDistance(i)
			}
		}

	}

}

func (sm *Starmap) BuildReachabilityMap(idx int, isStartingNode bool) {
	var dist, dist2 float64
	var lStart, lDest []int

	wn1 := sm.Stars[idx]
	wn1.Visited = true
	if isStartingNode {
		wn1.Distance = 0
		wn1.Reference = true
		_ = sm.AppendToPath(idx, idx)
	}
	lStart = append(lStart, idx)
	for {
		lDest = nil
		for zz := 0; zz < len(lStart); zz++ {
			idx = lStart[zz]
			wn1 = sm.Stars[idx]
			wn1Path := sm.Reachability[idx]
			wn1.Visited = true
			for i := 0; i < len(wn1.NeighborsIdx); i++ {
				nNeighbor := wn1.NeighborsIdx[i]
				wn2 := sm.Stars[nNeighbor]
				if wn2Path, ok := sm.Reachability[nNeighbor]; ok {
					if len(wn2Path.Path) == 0 {
						for i2 := 0; i2 < len(wn1Path.Path); i2++ {
							sm.AppendToPath(nNeighbor, wn1Path.Path[i2])
						}
						sm.AppendToPath(nNeighbor, nNeighbor)
					} else {
						_ = sm.UpdateReachabilityVectorDistance(nNeighbor)
						dist = sm.Reachability[nNeighbor].Distance
						_ = sm.UpdateReachabilityVectorDistance(idx)
						dist2 = sm.Reachability[idx].Distance + wn1.DistanceFrom((&wn2.Coordinates))
						if dist2 < dist {
							var lst2 []int
							wn3Path := sm.Reachability[idx]
							for ix := 0; ix < len(wn1Path.Path); ix++ {
								lst2 = append(lst2, wn3Path.Path[ix])
							}
							lst2 = append(lst2, nNeighbor)
							for midx := 0; midx < len(sm.Stars); midx++ {
								//sm.mtx.Lock()
								_ = sm.Reachability[midx].SubstituteSubVector(lst2)
								//sm.mtx.Unlock()
							}
						}
					}
				} else {
					fmt.Printf("starmap wn2: %s has no reachability at neighbor %d\n", wn2.Name, nNeighbor)
				}
			}
			for i := 0; i < len(wn1.NeighborsIdx); i++ {
				nNeighbor := wn1.NeighborsIdx[i]
				wn2 := sm.Stars[nNeighbor]
				if !(wn2.Visited) {
					hasNeighbor := false
					for _, ix := range lDest {
						if ix == nNeighbor {
							hasNeighbor = true
							break
						}
					}
					if !hasNeighbor {
						lDest = append(lDest, nNeighbor)
					}
				}
			}
		}
		lStart = nil
		for _, iTmp := range lDest {
			lStart = append(lStart, iTmp)
		}
		lDest = nil
		if len(lStart) == 0 {
			break
		}
	}

}

func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
